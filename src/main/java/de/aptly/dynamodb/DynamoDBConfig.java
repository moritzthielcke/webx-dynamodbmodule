/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.aptly.dynamodb;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClientBuilder;

/**
 *
 * @author moritz
 */
public class DynamoDBConfig {
    private String endpoint = "http://localhost:8000";
    private String region = "eu-central-1";
    private String awsKey = "key";
    private String awsSecret = "secret";
    
    public DynamoDBConfig(){

    }

    public DynamoDBConfig(String endpoint, String region, String awsKey, String awsSecret) {
        this.endpoint = endpoint;
        this.region = region;
        this.awsKey = awsKey;
        this.awsSecret = awsSecret;
    }
    
    public EndpointConfiguration endPointConfiguration(){
        return new AmazonDynamoDBAsyncClientBuilder.EndpointConfiguration(this.endpoint, this.region);
    }
    
    public AWSCredentialsProvider credentialsProvider(){
        return new AWSCredentialsProvider() {
            @Override
            public AWSCredentials getCredentials() {
                return new AWSCredentials() {
                    @Override
                    public String getAWSAccessKeyId() {
                       return awsKey;
                    }
                    @Override
                    public String getAWSSecretKey() {
                        return awsSecret;
                    }
                };
            }
            @Override
            public void refresh() {  }
        };          
    }
    

    
    
    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAwsKey() {
        return awsKey;
    }

    public void setAwsKey(String awsKey) {
        this.awsKey = awsKey;
    }

    public String getAwsSecret() {
        return awsSecret;
    }

    public void setAwsSecret(String awsSecret) {
        this.awsSecret = awsSecret;
    }
    
    
    
}
