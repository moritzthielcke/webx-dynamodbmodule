/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.aptly.dynamodb;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
public class DynamoDBSchemaBuilder {
    private static Logger logger = LogManager.getLogger(DynamoDBSchemaBuilder.class);
    private DynamoDB dynamoDB;
    private final Set<String> tableNames;
    private final Map<String, List<KeySchemaElement>> keySchemas = new HashMap<>();
    private final Map<String, List<AttributeDefinition>> attributes = new HashMap<>();
    private final Map<String, ProvisionedThroughput> provisions = new HashMap<>();
    //default provisioned throughput if not defined for the table itself    
    private ProvisionedThroughput provision =  new ProvisionedThroughput().withReadCapacityUnits(10L).withWriteCapacityUnits(10L);
    
    
    public DynamoDBSchemaBuilder(DynamoDB db){
        this(db, new HashSet<>());
    }
    
    public DynamoDBSchemaBuilder(DynamoDB db, Set<String> tableNames){
        this.dynamoDB = db;
        this.tableNames = tableNames;
    }
    
    public DynamoDBSchemaBuilder withTable(String tableName){
        this.tableNames.add(tableName);
        return this;
    }    
    
    
    /***
     * builds your schema , without dropping table
     */
    public void buildSchema(){
        buildSchema(false);
    }
    
    /***
     * 
     * @param dropTables drop existing tables?
     */
    public void buildSchema(boolean dropTables){
        if(dropTables){
            for(String tableName : tableNames){
                Table t = dynamoDB.getTable(tableName);
                if(t != null){
                    logger.info("dropping table "+tableName);
                    dynamoDB.getTable(tableName).delete();               
                }
            }
        }
        for(String tableName : tableNames){
            logger.info("creating schema for "+tableName);
            //todo no idea if attributes or keys can be empty here Oo
            
            List<KeySchemaElement> keySchema = this.keySchemas.get(tableName);
            if(keySchema==null){
               keySchema =  new ArrayList<>();
            }
            for(KeySchemaElement key : keySchema){
                logger.info(" - key -> "+key.getAttributeName()+" "+key.getKeyType());
            }
            CreateTableRequest createTable = new CreateTableRequest(tableName, keySchema);
            
            List<AttributeDefinition> attributes = this.attributes.get(tableName);
            if(attributes != null){
                for(AttributeDefinition attr : attributes){
                    logger.info(" - attribute -> "+attr.getAttributeName()+" "+attr.getAttributeType());
                }
                createTable.setAttributeDefinitions(attributes);
            }
            ProvisionedThroughput throughPut = this.provisions.get(tableName);
            if(throughPut==null){
                throughPut = this.provision;
            }
            createTable.setProvisionedThroughput(throughPut);
            dynamoDB.createTable(createTable);
            logger.info("table "+tableName+" created. r/w = "+throughPut.getReadCapacityUnits()+"/"+throughPut.getWriteCapacityUnits());
        }
    } 
    
    
    /****
     * sets the provisioned thoughput for all tables
     * @param p
     * @return 
     */
    public DynamoDBSchemaBuilder withProvisionedThroughput(ProvisionedThroughput p){
        this.provision = p;
        return this;
    }
    
    /***
     * the provisioned thoughput for a specific table
     * @param tableName
     * @param p
     * @return 
     */
    public DynamoDBSchemaBuilder withProvisionedThroughput(String tableName , ProvisionedThroughput p){
        this.provisions.put(tableName, p);
        return this;
    }
        
    /***
     * adds a key attribute to ALL tables
     * @param key
     * @param keyType
     * @param attributeName
     * @param attributeType
     * @return 
     */
    public DynamoDBSchemaBuilder withKeyAttribute(String attributeName, KeyType keyType, String attributeType){
        tableNames.forEach((tableName) -> {
            withKeyAttribute(tableName, attributeName, keyType, attributeType);
        });
        return this;
    }
    
    /***
     * adds a key attribute to a specific table
     * @param tableName
     * @param key
     * @param keyType
     * @param attributeName
     * @param attributeType
     * @return 
     */
    public DynamoDBSchemaBuilder withKeyAttribute(String tableName, String attributeName, KeyType keyType, String attributeType){
        if(keySchemas.get(tableName)==null){
            keySchemas.put(tableName, new ArrayList<>());
        }
        keySchemas.get(tableName).add(new KeySchemaElement(attributeName, keyType));
        withAttribute(tableName, attributeName, attributeType);
        return this;
    }
    
    
    /****
     * adds an attribute to all tables
     * @param attributeName
     * @param attributeType
     * @return 
     */
     public DynamoDBSchemaBuilder withAttribute(String attributeName, String attributeType){
         for(String tableName : tableNames){
             withAttribute(tableName, attributeName, attributeType);
         }
         return this;
     }   
    
    
    /****
     * adds an attribute to a specific table
     * @param tableName
     * @param attributeName
     * @param attributeType
     * @return 
     */
    public DynamoDBSchemaBuilder withAttribute(String tableName, String attributeName, String attributeType){
        if(attributes.get(tableName)==null){
            attributes.put(tableName, new ArrayList<>());
        }        
        attributes.get(tableName).add(new AttributeDefinition(attributeName, attributeType));
        return this;
    }    
    
    
    
}
