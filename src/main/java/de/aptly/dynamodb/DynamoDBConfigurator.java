/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.aptly.dynamodb;


import com.amazonaws.ClientConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.google.inject.AbstractModule;
import io.vertx.core.json.JsonObject;
import io.wx.core3.config.AbstractConfigurator;
import io.wx.core3.config.AppConfig;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
public class DynamoDBConfigurator extends AbstractConfigurator{
    private static Logger logger = LogManager.getLogger(DynamoDBConfigurator.class);
    private DynamoDBConfig dynamoCfg;
    private boolean useAddonCredentials=true;
           
    
    public DynamoDBConfigurator(){
        this(new DynamoDBConfig(), true);
    }
    
    public DynamoDBConfigurator(DynamoDBConfig dynamoCfg, Boolean useAddonCredentials){
        this.dynamoCfg = dynamoCfg;
        this.useAddonCredentials = useAddonCredentials;
    }
    
    @Override
    public void doConfig(Map modules) {
        final AppConfig cfg = getAppConfig();
        
        if(useAddonCredentials){
            JsonObject credentials = cfg.getAddonCredentials().getJsonObject("dynamodb");        
            if(credentials == null){
                logger.warn("cant find addon credentials for dynamodb !! Using defaults");
            }
            else{
                if(credentials.getString("endpoint") != null){
                    dynamoCfg.setEndpoint(credentials.getString("endpoint"));
                }
                if(credentials.getString("region") != null){
                    dynamoCfg.setRegion(credentials.getString("region"));
                }
                if(credentials.getString("secret") != null){
                    dynamoCfg.setAwsSecret(credentials.getString("secret"));                    
                }
                if(credentials.getString("key") != null){
                     dynamoCfg.setAwsKey(credentials.getString("key"));                     
                }
            }
        }
        
        modules.put("dynamodb", new AbstractModule() {
            @Override
            protected void configure() {       
                logger.info("Connecting to DynamoDB : "+dynamoCfg.getAwsKey()+"@"+dynamoCfg.getEndpoint()+ " "+dynamoCfg.getRegion()+" !");
                ClientConfiguration cc = new ClientConfiguration();
                cc.setConnectionTimeout( (12 * 1000) );
                AmazonDynamoDBAsyncClientBuilder builder = AmazonDynamoDBAsyncClientBuilder
                                                            .standard()
                                                            .withClientConfiguration(cc)
                                                            .withEndpointConfiguration( dynamoCfg.endPointConfiguration() )
                                                            .withCredentials( dynamoCfg.credentialsProvider() );
                AmazonDynamoDBAsync dynamoDB = builder.build();
                //DynamoDBMapper mapper = new DynamoDBMapper(dynamoDB)
                //DynamoDBMapper mapper = n;
                bind(AmazonDynamoDBAsync.class).toInstance( dynamoDB );
                bind(DynamoDBMapper.class).toInstance( new DynamoDBMapper(dynamoDB) );
            }
        });        
    }
    
}
